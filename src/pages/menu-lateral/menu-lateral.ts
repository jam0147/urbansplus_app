import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { UsuarioProvider } from '../../providers/usuario/usuario';

@Component({
  selector: 'page-menu-lateral',
  templateUrl: 'menu-lateral.html',
})
export class MenuLateralPage {

  constructor(public navCtrl: NavController, 
              private _us: UsuarioProvider) {}

  cerrar_sesion(){
    this._us.cerrar_sesion();
  }
}
