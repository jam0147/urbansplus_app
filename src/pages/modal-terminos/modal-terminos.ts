import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-modal-terminos',
  templateUrl: 'modal-terminos.html',
})
export class ModalTerminosPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}
}
