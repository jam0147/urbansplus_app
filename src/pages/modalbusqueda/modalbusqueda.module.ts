import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalbusquedaPage } from './modalbusqueda';

@NgModule({
  declarations: [
    ModalbusquedaPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalbusquedaPage),
  ],
})
export class ModalbusquedaPageModule {}
