import { Component, ErrorHandler, NgModule } from '@angular/core';
import { NavController, NavParams , AlertController} from 'ionic-angular';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { GuardServiceProvider } from './../../providers/guard-service/guard-service';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ApiProvider } from '../../providers/api/api';
import { FormsModule } from '@angular/forms';
import { PerfilPage } from '../perfil/perfil';

@Component({
  selector: 'page-cambiarclave',
  templateUrl: 'cambiarclave.html',
})
export class CambiarclavePage {
  public formData = {
      old_password    : "",
      password        : "",
      c_password      : ""
  }

  constructor(
      public navCtrl: NavController, 
      public navParams: NavParams, 
      public http: HttpClient,
      public auth: AuthServiceProvider,
      public api: ApiProvider,
      public alertCtrl: AlertController
      ) {
      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CambiarclavePage');
  }
  saveClave(){
    if(this.formData.password !== this.formData.c_password){
      let alert = this.alertCtrl.create({
            title: 'Error de validación',
            subTitle: 'La contraseña nueva debe de coincidir con el campo repita su contraseña!!!',
            buttons: ["ok"]
          });

      alert.present();
      return;
    }

    var $enviar = this.api.post('clave_update', this.formData).subscribe(
      result => {
        if (result.code != 200) {
          let alert = this.alertCtrl.create({
            title: 'Contraseña actualizada',
            subTitle: 'La Operación fue exitosa!!!',
            buttons: ["ok"]
          });

          alert.present();
          this.navCtrl.setRoot(PerfilPage);
        
        } else {
          
          
          
        }
      },
      error => {
        
        if(<any>error.status == 401){
          let alert = this.alertCtrl.create({
            title: 'Error de validación',
            subTitle: 'Error la contraseña actual no coinciden!!',
            buttons: ["ok"]
          });

          alert.present();
          return;
        }
        
       }
    );
    return $enviar;
  }

}
