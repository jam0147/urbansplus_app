import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CambiarclavePage } from './cambiarclave';

@NgModule({
  declarations: [
    CambiarclavePage,
  ],
  imports: [
    IonicPageModule.forChild(CambiarclavePage),
  ],
})
export class CambiarclavePageModule {}
