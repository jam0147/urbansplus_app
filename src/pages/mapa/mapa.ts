import { LoginPage } from './../login/login';

import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, LoadingController, ModalController, AlertController, Slides, Platform  } from 'ionic-angular';

import { UsuarioProvider } from '../../providers/usuario/usuario';
import { ApiProvider } from '../../providers/api/api';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

import { ConfiguracionesProvider } from '../../providers/configuraciones/configuraciones';

import { Geolocation } from '@ionic-native/geolocation';

import { AyudaPage } from "../ayuda/ayuda";
import { PerfilPage } from "../perfil/perfil";
import { MisviajesPage } from "../misviajes/misviajes";

import { vehiculoInterface } from '../../interfaces/vehiculo.interface';
import { viajeInterface } from '../../interfaces/viaje.interface';
import { formaPagoInterface } from '../../interfaces/formaPago.interface';

declare var google;
declare var directionsDisplay;
declare var directionsService;

@Component({
  selector: 'page-mapa',
  templateUrl: 'mapa.html',
})
export class MapaPage implements OnInit {

  @ViewChild(Slides) slides: Slides;

  map: any;
  mapObj = null;
  myLatLng:any;
  mapEle: HTMLElement;
  input_autocomplete : HTMLElement;
  autocomplete:any;
  direccionServ:any;
  direccionDisplay: any;
  autoCompleteValue:any;
  localizacionUsuario: any;
  buscando:boolean = false;

  vehiculos:vehiculoInterface[];

  viaje: viajeInterface = {
    id          : null,
    key          : null,
    fecha       : null,
    usuarioToken: null,
    usuarioID   : null,
    solicitud_tipo_vehiculo_id: null,
    inic_coord_inicio: null,
    inic_coord_final : null,
    status     : null,
    msj_conductor: null,
    distanciaTotal: null,
    tiempoTotal : null,
    tipos_pagos_id   : null,
    lugar_destino   : null
  };

  formas_pago: formaPagoInterface[];

  perfilPage: any = PerfilPage;
  ayudaPage: any = AyudaPage;
  misViajesPage: any = MisviajesPage;

  /* grales  */

  simboloMoneda: string = '$';


  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    private _us: UsuarioProvider,
    private geolocation: Geolocation,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public api: ApiProvider,
    public auth: AuthServiceProvider,
    public platform: Platform,
    public config: ConfiguracionesProvider) {

      this.vehiculos = this.config.getListaVehiculos();
      this.formas_pago = this.config.getListaTiposPagos();
    }

  ngOnInit() {
    this.platform.ready().then(() => {
      this.loadMap();
    });

  }

  cerrar_sesion() {
    this.auth.logout();
    this.navCtrl.setRoot(LoginPage);
  }

  async loadMap() {
    var self = this;

    const loading = await this.loadingCtrl.create();

    loading.present();

    this.myLatLng =  await this.getLocation();
    console.log(this.myLatLng);

    this.mapEle = document.getElementById('map');
    let mapOptions = {
      center: this.myLatLng,
      zoom: 12,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      fullscreenControl : false
    }

    var directionsDisplay = new google.maps.DirectionsRenderer;
    var directionsService = new google.maps.DirectionsService;

    this.direccionDisplay = directionsDisplay;
    this.direccionServ = directionsService;

    this.mapObj = new google.maps.Map(this.mapEle, mapOptions);

    this.direccionDisplay.setMap(this.mapObj);

    this.initAutocomplete();

    loading.dismiss();

    google.maps.event.addListener(directionsDisplay, 'directions_changed', function () {
      self.buscando = true;
      console.log("cambio");
    });

  }

  private addMaker(lat: number, lng: number) {
    const marker = new google.maps.Marker({
      position: { lat, lng },
      map: this.mapObj,
      title: 'Hello World!'
    });

    return marker;
  }

  private async getLocation() {

    const rta = await this.geolocation.getCurrentPosition({ timeout: 30000 });

    return {
      lat       : rta.coords.latitude,
      lng       : rta.coords.longitude,
      accuracy  : rta.coords.accuracy
    };
  }

  initAutocomplete(){
    this.input_autocomplete = document.getElementById('input_autocomplete');

    var options = {
      //componentRestrictions: { country: 've' }
    };

    this.autocomplete = new google.maps.places.Autocomplete(this.input_autocomplete, options);

    var localizacionUsuario = this.geolocateAutocomplete();
    var self = this;
    var autoC = this.autocomplete;
    var mapaObj = this.mapObj;

    this.autocomplete.addListener('place_changed', function () {
      var place = autoC.getPlace();
      let inputField = document.getElementById("input_autocomplete") as HTMLInputElement;

      var valor = inputField.value;

      if (!place.geometry) {
        window.alert("No se ha conseguido: '" + place.name + "'");
        return;
      }

      self.calculateAndDisplayRoute(self.localizacionUsuario, valor);
    });
  }

  calculateAndDisplayRoute(origen, destino) {
    let selectedMode = 'DRIVING';

    this.viaje.inic_coord_inicio = origen;
    this.viaje.inic_coord_final = destino;
    this.viaje.lugar_destino = destino;

    var directionsService = this.direccionServ;
    var directionsDisplay = this.direccionDisplay;
    var self = this;

    directionsService.route({
      origin: origen,
      destination: destino,
      travelMode: google.maps.TravelMode[selectedMode]
    }, function (response, status) {
      if (status == 'OK') {
        self.calcularViaje(response);
        directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }

  async geolocateAutocomplete() {
    const rta = await this.getLocation();

    var circle = new google.maps.Circle({
      center: rta,
      radius: rta.accuracy
    });

    this.autocomplete.setBounds(circle.getBounds());

    this.autocomplete.bindTo('bounds', this.mapObj);

    this.autocomplete.setOptions({ strictBounds: true });

    this.localizacionUsuario = rta;

    return rta;
  }

  seleccionarVehiculo(vehiculo: vehiculoInterface){
    vehiculo.seleccionado = !vehiculo.seleccionado;

    this.viaje.solicitud_tipo_vehiculo_id = vehiculo.id_tipo;

    this.slides.slideTo(1);
  }

  seleccionarPago(pago: formaPagoInterface){
    this.viaje.tipos_pagos_id = pago.id;
  }

  isSelectedVehiculo(vehiculo: vehiculoInterface){
    if(vehiculo.id_tipo == this.viaje.solicitud_tipo_vehiculo_id)
      return true;

    return false;
  }

  isSelectedPago(pago: formaPagoInterface) {
    return pago.id == this.viaje.tipos_pagos_id;
  }

  precioFormateado(precio:number){
    return new Intl.NumberFormat().format(precio * this.viaje.distanciaTotal);
  }


  solicitarViaje(){
    var error = this.validarFormulario();

    if(error != ''){
      let alert = this.alertCtrl.create({
        title: 'Validación',
        subTitle: error,
        buttons: ["ok"]
      });

      alert.present();

      return false;
    }

    const confirm = this.alertCtrl.create({
      title: 'Confirmar Solicitud',
      message: '¿Está seguro/a de enviar ésta solicitud?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            confirm.dismiss();
          }
        },
        {
          text: 'Solicitar',
          handler: () => {
            this.enviarSolicitud();
          }
        }
      ]
    });
    confirm.present();

  }

  escribirAutoc(){
    this.buscando = false;
  }

  abrirPagina(pagina: any){
    this.navCtrl.push(pagina);
  }

  calcularViaje(directionsResult: any){
    let distanciaTotal = 0;
    let tiempoTotal = 0;
    var legs = directionsResult.routes[0].legs;

    for (var i = 0; i < legs.length; ++i) {
      if(i == 0)
        this.viaje.inic_coord_final = legs[i].start_location;

      distanciaTotal += legs[i].distance.value;
      tiempoTotal += legs[i].duration.value;
    }

    this.viaje.distanciaTotal = (distanciaTotal / 1000);
    this.viaje.tiempoTotal = tiempoTotal;
  }

  validarFormulario(){
    if(this.viaje.solicitud_tipo_vehiculo_id == null){
      this.slides.slideTo(0);
      return 'Selecciona el vehículo';
    }

    if (this.viaje.tipos_pagos_id == null) {
      return 'Selecciona el tipo de pago';
    }

    return '';
  }

  enviarSolicitud(){
    const loading = this.loadingCtrl.create({
      content: "Espere por favor..."
    });

    loading.present();

    var $respuesta = this.api.post('solicitud_viaje', this.viaje).subscribe(
      result => {
        if (result.code != 200) {
          let alert = this.alertCtrl.create({
            title: 'Solicitud enviada',
            subTitle: 'Se ha enviado la solicitus satisfactoriamente',
            buttons: ["ok"]
          });

          alert.present();
          this.navCtrl.push(MisviajesPage);
          return false;
        } else {
          this.navCtrl.push(MisviajesPage);
        }
      },
      error => {
        console.log(<any>error);

        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'Ha ocurrido un error en la solicitud del viaje :(',
          buttons: ["ok"]
        });

        alert.present();
      }
    );

    loading.dismiss();
  }
}
