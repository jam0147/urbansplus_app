import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { ModalfaqRespuestasPage } from "../modalfaq-respuestas/modalfaq-respuestas";

@Component({
  selector: 'page-ayuda',
  templateUrl: 'ayuda.html',
})
export class AyudaPage {

  preguntas : any[] = [
    {
      pregunta  : 'Pregunta nro 1',
      respuesta : 'Respuesta a la Pregunta nro 1'
    },
    {
      pregunta: 'Pregunta nro 2',
      respuesta: 'Respuesta a la Pregunta nro 2'
    },
    {
      pregunta: 'Pregunta nro 3',
      respuesta: 'Respuesta a la Pregunta nro 3'
    }
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl : ModalController) {
  }

  verPregunta(pregunta:any){
    let modal = this.modalCtrl.create(ModalfaqRespuestasPage, pregunta).present();

  }
 
}
