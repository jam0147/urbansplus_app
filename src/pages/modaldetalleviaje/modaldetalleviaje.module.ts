import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModaldetalleviajePage } from './modaldetalleviaje';

@NgModule({
  declarations: [
    ModaldetalleviajePage,
  ],
  imports: [
    IonicPageModule.forChild(ModaldetalleviajePage),
  ],
})
export class ModaldetalleviajePageModule {}
