import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

import { viajeInterface } from '../../interfaces/viaje.interface';
import { ApiProvider } from '../../providers/api/api';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { ConfiguracionesProvider } from '../../providers/configuraciones/configuraciones';

/* Firebase */
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';

@Component({
  selector: 'page-modaldetalleviaje',
  templateUrl: 'modaldetalleviaje.html',
})
export class ModaldetalleviajePage {
  // Definiciones 
  listaStatus: any;
  listaVehiculos: any;

  buscando:boolean = true;
  viaje: viajeInterface;
  viaje_status: any;
  viaje_vehiculo: any;
  

  input_msj: string;
  enviando : boolean = false;

  public chats: Observable<any[]>;
  public chatRef: AngularFireList<any>;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public api: ApiProvider, 
              public alertCtrl: AlertController,
              public database: AngularFireDatabase,
              public us: UsuarioProvider,
              public config: ConfiguracionesProvider,
              private payPal: PayPal) {
    
    this.viaje = this.navParams.get('viaje');
    this.viaje_status =  this.config.getInfoStatus(this.viaje.status);
    this.viaje_vehiculo = this.config.getInfoVehiculo(this.viaje.solicitud_tipo_vehiculo_id);

    this.chatRef = this.database.list('chat/' + this.viaje.key);
    this.chats = this.chatRef.snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      });
  }

  ionViewDidLoad() {
    this.buscando = false;
  }

  cancelarViajeUsuario(viaje_id){
    const confirm = this.alertCtrl.create({
      title: 'Confirmar',
      message: '¿Está seguro/a de cancelar ésta solicitud?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            confirm.dismiss();
          }
        },
        {
          text: 'Si, cancelar solicitud',
          handler: () => {
            this._cancelarViajeUsuario(viaje_id);
          }
        }
      ]
    });

    confirm.present();
  }

  _cancelarViajeUsuario(viaje_id){
    var $respuesta = this.api.post('cancelar_viaje', {
      viajes_id: viaje_id
    }).subscribe(
      result => {
        let alert = this.alertCtrl.create({
          title: 'Cancelar Viaje',
          subTitle: 'Su solicitud ha sido cancelada',
          buttons: ["ok"]
        });

        this.navCtrl.pop();
      },
      error => {
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'Ha ocurrido un error en la cancelación del viaje :(',
          buttons: ["ok"]
        });

        alert.present();
      }
    );

    return $respuesta;
  }

  getClassMsjChat(msj){
    //return msj.user_id == this.us.infoUser.id ? 'msj_usuario' : 'msj_destinatario';
    return msj.user_id == 1 ? 'msjchat msj_usuario' : 'msjchat msj_destinatario';
  }

  enviarMsj(){
    if (this.input_msj == '' || this.input_msj == undefined || this.enviando)
      return false;

    this.enviando = true;

    var $respuesta = this.api.post('enviar_Mensaje', {
      viajes_id : this.viaje.key,
      mensaje   : this.input_msj
    }).subscribe(
      result => {
        this.enviando = false;
        this.input_msj = "";
      },
      error => {
        this.enviando = false;

        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'No se envió su mensaje :(',
          buttons: ["ok"]
        });

        alert.present();
      }
    );
  }

  pagarPaypal(viaje: viajeInterface){
    this.payPal.init({
      PayPalEnvironmentProduction: '',
      PayPalEnvironmentSandbox: 'Abx-j3ht1mr2xdYh-NfxBxh7mKx6lzhQlXrZH5bJNHRhiOs8-tZ7Dw0EpCXIn4dsyDPFqWSfKotGOfR2'
    }).then(() => {
      // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
      this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
        // Only needed if you get an "Internal Service Error" after PayPal login!
        //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
      })).then(() => {
        let payment = new PayPalPayment('3.33', 'USD', 'Description', 'sale');
        this.payPal.renderSinglePaymentUI(payment).then(() => {
          // Successfully paid

          // Example sandbox response
          //
          // {
          //   "client": {
          //     "environment": "sandbox",
          //     "product_name": "PayPal iOS SDK",
          //     "paypal_sdk_version": "2.16.0",
          //     "platform": "iOS"
          //   },
          //   "response_type": "payment",
          //   "response": {
          //     "id": "PAY-1AB23456CD789012EF34GHIJ",
          //     "state": "approved",
          //     "create_time": "2016-10-03T13:33:33Z",
          //     "intent": "sale"
          //   }
          // }
        }, () => {
          // Error or render dialog closed without being successful
        });
      }, () => {
        // Error in configuration
      });
    }, () => {
      // Error in initialization, maybe PayPal isn't supported or something else
    });
  }
}


/*
Sandbox account
johansymg-facilitator@gmail.com

Client ID
Abx-j3ht1mr2xdYh-NfxBxh7mKx6lzhQlXrZH5bJNHRhiOs8-tZ7Dw0EpCXIn4dsyDPFqWSfKotGOfR2

secret
ECf-quaQUIj1aJSZvH9XudRGeJSZ9DD_Jj5rcj0OAAbLCenVSez9eytuPNFVAs0-G_ADF6W4sGDQbCm7
*/