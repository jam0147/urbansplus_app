import { GuardServiceProvider } from './../../providers/guard-service/guard-service';
import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  isLoggedIn: boolean;

  constructor(
    public navCtrl: NavController,
    public auth: AuthServiceProvider,
    public guard: GuardServiceProvider
  ) {}

  ionViewCanEnter() {
    if(this.guard.isLoggedIn){
      this.isLoggedIn = this.guard.isLoggedIn;
      return true;
    } else {
      this.navCtrl.push(LoginPage);
    }
  }

  goToLogin() {
    this.navCtrl.push(LoginPage);
  }

  logout() {
    this.auth.logout();
    this.navCtrl.push(LoginPage);
  }

}
