import { Component, ErrorHandler, NgModule } from '@angular/core';
import { NavController, NavParams, AlertController, ActionSheetController, ToastController, Platform, LoadingController, Loading } from 'ionic-angular';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { GuardServiceProvider } from './../../providers/guard-service/guard-service';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ApiProvider } from '../../providers/api/api';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { userInterface } from '../../interfaces/user.interface';

import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { FormsModule } from '@angular/forms';
import { PerfilPage } from '../perfil/perfil';
@Component({
  selector: 'page-misdatos',
  templateUrl: 'misdatos.html',
})
export class MisdatosPage {
  public headers;
  userToken = localStorage.getItem('user');
  datos : {};
  datosUsuario: userInterface;
  image:string = null;
  lastImage: string = null;
  loading: Loading;
  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public http: HttpClient,
      public auth: AuthServiceProvider,
      public api: ApiProvider,
      public user : UsuarioProvider,
      public alertCtrl: AlertController,
      private camera : Camera,
      private file: File,
      private transfer: FileTransfer,
      public actionSheetCtrl: ActionSheetController,
      public loadingCtrl: LoadingController


      ) {
        this.datos = {
          token : this.userToken
        };
        this.datosUsuario = this.user.infoUser;
        console.log(this.datosUsuario);
  }

  ionViewDidLoad() {
    this.obtenermisDatos();
  }
  getPicture(){
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    const optionsLibrary: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum:false,
      allowEdit:true,
      targetWidth:300,
      targetHeight:300
    }
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Seleccionar una Imagen',
      buttons: [
        {
          text: 'Cargar desde galeria',
          handler: () => {
            this.camera.getPicture(optionsLibrary).then((imageData) => {

            this.image = 'data:image/jpeg;base64,' + imageData;
            }, (err) => {

            });
          }
        },
        {
          text: 'Usar Camara',
          handler: () => {
            this.camera.getPicture(options).then((imageData) => {

            this.image = 'data:image/jpeg;base64,' + imageData;
            }, (err) => {

            });
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();

  }

  obtenermisDatos() {
    var $respuesta = this.api.post('perfil', this.datos).subscribe(
      result => {
        if (result.code != 200) {
          this.datosUsuario = result.perfil;

        } else {
          console.log('error');
        }
      },
      error => {
        console.log(<any>error);
       }
    );
    return $respuesta;
  }
  saveDatos() {
    var $enviar = this.api.post('perfil_update', this.datosUsuario).subscribe(
      result => {
        if (result.code != 200) {
          let alert = this.alertCtrl.create({
            title: 'Datos actualizados',
            subTitle: 'La Operación fue exitosa!!!',
            buttons: ["ok"]
          });

          alert.present();
          this.navCtrl.setRoot(PerfilPage);

        } else {
          console.log('error');
        }
      },
      error => {
        console.log(<any>error);
       }
    );
    return $enviar;
  }

  uploadImage(){
    //Show loading
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();

    //create file transfer object
    const fileTransfer: FileTransferObject = this.transfer.create();

    //random int
    var random = Math.floor(Math.random() * 100);

    //option transfer
    let options: FileUploadOptions = {
      fileKey: 'photo',
      fileName: "myImage_" + random + ".jpg",
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: "image/jpeg",
      headers: this.headers = {
        'Authorization': 'Bearer ' + this.user.getTokenUser()
      }
    }

    //file transfer action
    fileTransfer.upload(this.image, this.api.url_base_api + '/subsubir_imagen', options)
      .then((data) => {
        alert("Success");
        loader.dismiss();
      }, (err) => {
        console.log(err);
        alert("Error");
        loader.dismiss();
      });
  }

}
