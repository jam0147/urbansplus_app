import { paisInterface } from './../../interfaces/pais.interface';
import { GuardServiceProvider } from './../../providers/guard-service/guard-service';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

import { MapaPage } from './../mapa/mapa';
import { RegistrousuarioPage } from './../registrousuario/registrousuario';
import { RestauracionPage } from './../restauracion/restauracion';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  registroUsuarioPage = RegistrousuarioPage;
  restauracionPage = RestauracionPage;

  public formData = {
      name: "",
      surname: "",
      email: "",
      password: "",
      password_confirmation : "",
      terms: false,
      pais_id: ""
    }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthServiceProvider,
    public loadingCntrl: LoadingController,
    public alertCntrl: AlertController,
    public guard: GuardServiceProvider,
    public ModalCtrl: ModalController    ) {
    }

  alert(title: string, subtitle: string, buttons) {
    let alert = this.alertCntrl.create({
      title: title,
      subTitle: subtitle,
      buttons: buttons
    });
    alert.present();
  }

  loading(message, $page = null) {
    const loader = this.loadingCntrl.create({
      content: message,
    });
    loader.present();
    if($page == null) {
      setTimeout(() => {
        loader.dismiss();
        this.navCtrl.push(LoginPage);
      }, 1000);
    } else {
      setTimeout(() => {
        loader.dismiss();
        this.navCtrl.setRoot($page);
      }, 1000);
    }
  }

  login() {
    if (this.formData.email == '' || this.formData.password == ''){
      this.alert("Login", 'El correo y la contraseña son obligatorios', ["ok"]);
      return false;
    }

    var self = this;
    this.auth.login(this.formData.email, this.formData.password)
    .subscribe(res => {
      console.log(res);
      this.loading("Successful login", MapaPage);
    }, error => {
      console.log(error);
      this.alert("Failed to login", error.message, ["ok"]);
    });
  }

  pagina(pagina: any){
    this.navCtrl.push(pagina);
  }
}
