import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViajesActualesPage } from './viajes-actuales';

@NgModule({
  declarations: [
    ViajesActualesPage,
  ],
  imports: [
    IonicPageModule.forChild(ViajesActualesPage),
  ],
})
export class ViajesActualesPageModule {}
