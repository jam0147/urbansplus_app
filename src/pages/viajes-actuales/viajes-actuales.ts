import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { ModaldetalleviajePage } from "../modaldetalleviaje/modaldetalleviaje";

import { ApiProvider } from '../../providers/api/api';
import { ConfiguracionesProvider } from '../../providers/configuraciones/configuraciones';

import { viajeInterface } from '../../interfaces/viaje.interface';

/* Firebase */
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-viajes-actuales',
  templateUrl: 'viajes-actuales.html',
})
export class ViajesActualesPage {

  public viajes: Observable<any[]>;
  public viajesRef: AngularFireList<any>;
  lista_status:any;

  constructor(
      public navCtrl: NavController, 
      public navParams: NavParams, 
      public api: ApiProvider, 
      public modalCtrl: ModalController,
      public database: AngularFireDatabase,
      public config: ConfiguracionesProvider) {

    this.lista_status = this.config.getListaStatus();
    
    this.viajesRef = this.database.list('viajes/' + '1');
    this.viajes = this.viajesRef.snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    });
  }

  buscarViaje(viaje: viajeInterface) {
    const modal = this.modalCtrl.create(ModaldetalleviajePage, { viaje: viaje });
    modal.present();
  }

  getClassItemStatus(status: number){
    return "item item-block item-md " + this.lista_status[status].clase;
  }

  getImgVehiculo(solicitud_tipo_vehiculo_id: number){
    return this.config._getImgVehiculo(solicitud_tipo_vehiculo_id);    
  }
}