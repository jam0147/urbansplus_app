import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { ModaldetalleviajePage } from "../modaldetalleviaje/modaldetalleviaje";

import { ApiProvider } from '../../providers/api/api';

import { viajeInterface } from '../../interfaces/viaje.interface';
import { LISTA_STATUS } from "../../data/status.data";

@Component({
  selector: 'page-viajes-historial',
  templateUrl: 'viajes-historial.html',
})
export class ViajesHistorialPage {

  public viajes: viajeInterface[] = [];
  lista_status = LISTA_STATUS;

  constructor(public navCtrl: NavController, public navParams: NavParams, public api: ApiProvider, public modalCtrl: ModalController) {
    this.buscarHistorialViajes();
  }

  buscarHistorialViajes(){
    var self = this;
    
    var $respuesta = this.api.post('historial_viajes', {}).subscribe(
      result => {
        if(result.s == 's'){
          self.viajes = result.viajes_activos;
        }
        
      },
      error => {
        
      }
    );
  }

  buscarViaje(viaje: viajeInterface){
    const modal = this.modalCtrl.create(ModaldetalleviajePage, {viaje: viaje});
    modal.present();
  }

  getClassItemStatus(status: number) {
    let $clase = "item item-block item-md ";
    let $status:any = "";

    $status = this.lista_status[status];
    $clase = $clase + $status.clase;

    return $clase;
  }
}
