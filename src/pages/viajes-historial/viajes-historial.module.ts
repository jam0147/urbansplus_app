import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViajesHistorialPage } from './viajes-historial';

@NgModule({
  declarations: [
    ViajesHistorialPage,
  ],
  imports: [
    IonicPageModule.forChild(ViajesHistorialPage),
  ],
})
export class ViajesHistorialPageModule {}
