import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-modalfaq-respuestas',
  templateUrl: 'modalfaq-respuestas.html',
})
export class ModalfaqRespuestasPage {

  pregunta: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.pregunta = {
      'pregunta': this.navParams.get('pregunta'),
      'respuesta': this.navParams.get('respuesta')
    };    
  }
}
