import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalfaqRespuestasPage } from './modalfaq-respuestas';

@NgModule({
  declarations: [
    ModalfaqRespuestasPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalfaqRespuestasPage),
  ],
})
export class ModalfaqRespuestasPageModule {}
