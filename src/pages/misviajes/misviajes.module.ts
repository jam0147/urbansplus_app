import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MisviajesPage } from './misviajes';

@NgModule({
  declarations: [
    MisviajesPage,
  ],
  imports: [
    IonicPageModule.forChild(MisviajesPage),
  ],
})
export class MisviajesPageModule {}
