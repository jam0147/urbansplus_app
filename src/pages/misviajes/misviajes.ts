import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

import { ViajesActualesPage } from "../viajes-actuales/viajes-actuales";
import { ViajesHistorialPage } from "../viajes-historial/viajes-historial";
import { UsuarioProvider } from '../../providers/usuario/usuario';

@Component({
  selector: 'page-misviajes',
  templateUrl: 'misviajes.html',
})
export class MisviajesPage {

  tabActuales: any;
  tabHistorial:any;

  datos : {};

  constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public auth: AuthServiceProvider,
        public api: ApiProvider,
        public user : UsuarioProvider,
        public alertCtrl: AlertController


        ) {
        this.tabActuales = ViajesActualesPage;
        this.tabHistorial= ViajesHistorialPage;

  }
}
