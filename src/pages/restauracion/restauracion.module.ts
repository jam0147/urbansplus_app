import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestauracionPage } from './restauracion';

@NgModule({
  declarations: [
    RestauracionPage,
  ],
  imports: [
    IonicPageModule.forChild(RestauracionPage),
  ],
})
export class RestauracionPageModule {}
