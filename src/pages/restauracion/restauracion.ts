import { Component } from '@angular/core';

import { NavController, NavParams, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { GuardServiceProvider } from './../../providers/guard-service/guard-service';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

import { LoginPage } from '../login/login';

@Component({
  selector: 'page-restauracion',
  templateUrl: 'restauracion.html',
})
export class RestauracionPage {

  public headers: HttpHeaders;

  public formData = {
    name: "",
    surname: "",
    email: "",
    password: "",
    password_confirmation: "",
    terms: false,
    pais_id: ""
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthServiceProvider,
    public loadingCntrl: LoadingController,
    public alertCntrl: AlertController,
    public guard: GuardServiceProvider,
    public ModalCtrl: ModalController,
    public http: HttpClient
  ) {
  }

  recuperar_contrasena() {
    this.auth.sendPasswordResetEmail(this.formData.email)
      .subscribe(res => {
        let $msj = "Siga las instrucciones que se han enviado a su correo electrónico";
        this.alertCntrl.create({
          title: "Urban's Plus",
          message: $msj,
          buttons: [{
            text: 'OK',
            handler: () => {
              this.navCtrl.pop();
            }
          }],

        }).present();
      }, error => {
        let $msj = "Ocurrio un error...";
        this.alertCntrl.create({
          title: "Urban's Plus",
          subTitle: $msj,
          buttons: ["Ok!"],
        }).present();
      });
  }

  ir_login(){
    this.navCtrl.pop();
  }

}
