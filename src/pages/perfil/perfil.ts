import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MisdatosPage } from "../misdatos/misdatos";
import { CambiarclavePage } from "../cambiarclave/cambiarclave";


@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
  }

  irMisDatos(){
    this.navCtrl.push(MisdatosPage);
  }

  irCambiarClave(){
    this.navCtrl.push(CambiarclavePage);
  }
}
