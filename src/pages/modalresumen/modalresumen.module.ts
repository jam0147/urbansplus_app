import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalresumenPage } from './modalresumen';

@NgModule({
  declarations: [
    ModalresumenPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalresumenPage),
  ],
})
export class ModalresumenPageModule {}
