import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { GuardServiceProvider } from './../../providers/guard-service/guard-service';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

import { ModalTerminosPage } from '../modal-terminos/modal-terminos';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-registrousuario',
  templateUrl: 'registrousuario.html',
})
export class RegistrousuarioPage {
  public headers: HttpHeaders;

  public formData = {
    name: "",
    surname: "",
    email: "",
    password: "",
    password_confirmation: "",
    terms: false,
    pais_id: ""

  }
  paises: Array<any>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthServiceProvider,
    public loadingCntrl: LoadingController,
    public alertCntrl: AlertController,
    public guard: GuardServiceProvider,
    public ModalCtrl: ModalController,
    public http: HttpClient
  ) {

    this.headers = new HttpHeaders();
    this.headers.append("Content-Type", 'application/json');
    this.headers.append("Access-Control-Allow-Origin", "*");
    this.headers.append("Access-Control-Allow-Headers", "Origin, Authorization, Content-Type, Accept");
    this.obtenerPaises();
  }
  
  obtenerPaises() {
    return this.http.get(this.auth.baseURL + 'obtener-paises', {
      headers: this.headers
    }).subscribe(response => {
      this.paises = response["paises"];
    });
  }

  mostrar_modal_terminos() {
    let modal = this.ModalCtrl.create(ModalTerminosPage);
    modal.present();
  }

  registrar() {
    this.loading("Por favor espere...")
    this.auth.register(
      this.formData.name,
      this.formData.surname,
      this.formData.email,
      this.formData.password,
      this.formData.password_confirmation,
      this.formData.pais_id)
      .subscribe((res) => {
        console.log(res.message);
      }, error => {
        console.log(error);
      });
  }
  
  loading(message, $page = null) {
    const loader = this.loadingCntrl.create({
      content: message,
    });
    loader.present();
    if ($page == null) {
      setTimeout(() => {
        loader.dismiss();
        this.navCtrl.push(LoginPage);
      }, 1000);
    } else {
      setTimeout(() => {
        loader.dismiss();
        this.navCtrl.setRoot($page);
      }, 1000);
    }
  }

  ir_login() {
    this.navCtrl.pop();
  }

}
