import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { LaunchNavigator } from '@ionic-native/launch-navigator';

import { Geolocation } from '@ionic-native/geolocation';

import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';

import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { MapaPage } from '../pages/mapa/mapa';
import { ModalTerminosPage } from '../pages/modal-terminos/modal-terminos';
import { AyudaPage } from "../pages/ayuda/ayuda";
import { PerfilPage } from "../pages/perfil/perfil";
import { MisdatosPage } from "../pages/misdatos/misdatos";
import { CambiarclavePage } from "../pages/cambiarclave/cambiarclave";
import { MisviajesPage } from "../pages/misviajes/misviajes";
import { ViajesActualesPage } from "../pages/viajes-actuales/viajes-actuales";
import { ViajesHistorialPage } from "../pages/viajes-historial/viajes-historial";
import { ModaldetalleviajePage } from "../pages/modaldetalleviaje/modaldetalleviaje";
import { ModalfaqRespuestasPage } from "../pages/modalfaq-respuestas/modalfaq-respuestas";
import { RestauracionPage } from "../pages/restauracion/restauracion";
import { RegistrousuarioPage  } from "../pages/registrousuario/registrousuario";

/* Firebase */

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { firebaseConfig } from '../config/firebase.config';

// Servicios
import { UsuarioProvider } from '../providers/usuario/usuario';

// Storage
import { IonicStorageModule } from '@ionic/storage';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';

import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { GuardServiceProvider } from '../providers/guard-service/guard-service';
import { ApiProvider } from '../providers/api/api';
//  Formularios
import { FormsModule } from '@angular/forms';

//Components
import { MapComponent  } from "../components/map/map";
import { ConfiguracionesProvider } from '../providers/configuraciones/configuraciones';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    MapaPage,
    ModalTerminosPage,
    AyudaPage,
    PerfilPage,
    MisdatosPage,
    CambiarclavePage,
    MisviajesPage,
    ViajesActualesPage,
    ViajesHistorialPage,
    ModaldetalleviajePage,
    ModalfaqRespuestasPage,
    MapComponent,
    RestauracionPage,
    RegistrousuarioPage
  ],
  imports: [
    BrowserModule, HttpModule, HttpClientModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    IonicStorageModule.forRoot(),
    FormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    MapaPage,
    ModalTerminosPage,
    AyudaPage,
    PerfilPage,
    MisdatosPage,
    CambiarclavePage,
    MisviajesPage,
    ViajesActualesPage,
    ViajesHistorialPage,
    ModaldetalleviajePage,
    ModalfaqRespuestasPage,
    MapComponent,
    RestauracionPage,
    RegistrousuarioPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    UsuarioProvider,
    LaunchNavigator,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    GuardServiceProvider,
    ApiProvider,
    Camera,
    FileTransfer,
    File,
    ConfiguracionesProvider,
    PayPal
    
  ]
})
export class AppModule {}
