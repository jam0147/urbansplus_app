export interface vehiculoInterface {
    id_tipo : number,
    nombre: string,
    tiempo: string,
    logo: string,
    factor_precio: number,
    seleccionado:boolean
}