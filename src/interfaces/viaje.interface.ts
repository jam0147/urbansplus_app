export interface viajeInterface {
    id                  : number,
    key                 : number,
    fecha               : string,
    usuarioToken        : string,
    usuarioID           : number,
    solicitud_tipo_vehiculo_id       : number,
    inic_coord_inicio        : string,
    inic_coord_final         : string,
    status             : number,
    msj_conductor       : string,
    distanciaTotal      : any,
    tiempoTotal         : any,
    tipos_pagos_id      : number,
    lugar_destino       : string
}