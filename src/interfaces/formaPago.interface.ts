export interface formaPagoInterface {
    id      : number,
    nombre  : string,
    logo    : string,
    activo  : boolean
}