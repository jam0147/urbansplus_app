export interface userInterface {
    id: number,
    name: string,
    surname: string,
    avatar: string,
    email: string,
    dni: string,
    direccion: string,
    tlf: string,
    pais: string,
    pais_code: string,
    estado:number
}
