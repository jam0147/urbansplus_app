import { Injectable } from '@angular/core';

/*
  Generated class for the GuardServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GuardServiceProvider {

  public isLoggedIn: boolean = false;

  constructor() {
    this.checkToken();
  }

  checkToken() : boolean {
    if(localStorage.getItem('user')){
      this.isLoggedIn = true;
      console.log("LoggedIn: "+this.isLoggedIn);
      return true;
    } else {
      this.isLoggedIn = false;
      console.log("LoggedIn: "+this.isLoggedIn);
      return false;
    }
  }



}
