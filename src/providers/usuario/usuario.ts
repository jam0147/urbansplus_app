import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Observable } from 'rxjs/Observable';

import { Storage } from '@ionic/storage';
import { Platform } from "ionic-angular";

import { userInterface}  from '../../interfaces/user.interface'

import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

let apiUrl = "http://urbansplus.oo/api/";

@Injectable()
export class UsuarioProvider {
  loggedChange: Subject<boolean> = new Subject<boolean>();

  items: Observable<any[]>;

  clave: string = "";
  correo: string = "";
  logged:boolean =  true;

  public infoUser: userInterface = {
    id: null,
    name: null,
    surname: null,
    avatar: null,
    email: null,
    dni: null,
    direccion: null,
    tlf: null,
    pais: null,
    pais_code: null,
    estado: 0
  };

  usuarios : AngularFireList<any>;

  constructor(private afDB: AngularFireDatabase,
              private storage: Storage,
              private platform: Platform,
              public http : Http) {

    this.loggedChange.subscribe((value) => {
      this.logged = value
    });


    this.setInfoUser();

  }

  verifica_usuario(correo: string, clave: string) {
    this.clave = clave.toLowerCase();
    this.correo = correo.toLowerCase();

    let login:any = {
      password: clave,
      email: correo
    }

    console.log(login);

    this.postData(login, 'login').then((response) => {
      console.log(response);
    }).catch((error)=>{
      console.error(error);
    });

    this.infoUser.name = 'Johansy';
    this.infoUser.surname = 'Mujica';
    this.infoUser.estado = 1;

    this.loggedChange.next(true);

    this.setStorage('token', 'TOKEN123');
    this.setStorage('logged', this.logged);

    return console.log(this.infoUser);



    /*let promesa = new Promise( (resolve, reject) =>{
      this.afDB.list('usuarios/' + this.clave )
          .subscribe( data => {
            console.log( data );

            resolve();
          });
    }).catch(
        error => console.log( "Error en promesa Service" )
    );

    return promesa;*/


  }

  cerrar_sesion(){
    this.loggedChange.next(false);

    this.setStorage('token', null);
    this.setStorage('logged', this.logged);
  }

  setStorage(opt, val){
    let promesa = new Promise( (resolve, reject) => {
      if(this.platform.is("cordova")){
        //dispositivo
        this.storage.set(opt, val);
      }else{
          localStorage.setItem(opt, val);
      }
    });

    return promesa;
  }

  getStorage(opt){
    let promesa = new Promise((resolve, reject) => {
      if (this.platform.is("cordova")) {
        this.storage.get(opt);
      } else {
        localStorage.getItem(opt);
      }
    });

    return promesa;
  }

  getusuarios(){
    console.log(this.afDB.list('usuarios'));
    return this.usuarios;
  }

  postData(credentials, type) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();

      this.http.post(apiUrl + type, JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
    });
  }

  getTokenUser(){
    return localStorage.getItem("user");
  }

  setLocalStorage(item: string, valor : any){
    localStorage.setItem(item, valor);
  }

  removeTokenUser() {
    localStorage.removeItem('user');
    localStorage.removeItem('user_info');
  }

  setInfoUser()
  {
    let info = JSON.parse(localStorage.getItem('user_info'));
    this.infoUser = info;
  }

}
