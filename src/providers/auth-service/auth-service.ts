import { UsuarioProvider } from './../usuario/usuario';
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import {
  Injectable
} from '@angular/core';
import {
  Observable
} from 'rxjs';
import {
  map
} from 'rxjs/operators';
import {
  Storage
} from '@ionic/storage';
/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthServiceProvider {

  public token: string;
  public headers: HttpHeaders;
  //public baseURL = "http://localhost/urbansplus/public/api/";
  public baseURL = "http://urbansplus.com/api/";
  public loginEndPoint = this.baseURL + "login";
  public logoutEndPoint = this.baseURL + "logout";
  public registerEndPoint = this.baseURL + "register";
  public sendPasswordEndPonit = this.baseURL + "restaurar";
  public resetPasswordEndPoint = this.baseURL + "reset-password";

  constructor(public http: HttpClient, public storage: Storage, public u: UsuarioProvider) {

    this.headers = new HttpHeaders();
    this.headers.append("Content-Type", 'application/json');
    this.headers.append("Access-Control-Allow-Origin", "*");
    this.headers.append("Access-Control-Allow-Headers", "Origin, Authorization, Content-Type, Accept");
    //this.headers.append("Authorization", "Bearer " + this.token);
    //var currentUser = JSON.parse(localStorage.getItem('user'));
    //this.token = currentUser && currentUser.token;
  }

  login(email: string, password: string): Observable < any > {
    return this.http.post(this.loginEndPoint, {
        email: email.toLowerCase(),
        password: password.toLowerCase()
      }, {
        headers: this.headers
      })
      .pipe(
        map((response: Response) => {
          this.token = response['access_token'];
        
          if (this.token) {
            this.u.setLocalStorage('user', this.token);
            
            
            this.u.setLocalStorage('user_info', JSON.stringify(response['user']));
            
            this.u.infoUser.id = response['user']['id'];
            this.u.infoUser.name = response['user']['name'];
            this.u.infoUser.surname = response['user']['surname'];
            this.u.infoUser.email = response['user']['email'];
            this.u.infoUser.dni = response['user']['dni'];
            this.u.infoUser.avatar = response['user']['avatar'];
            this.u.infoUser.pais_code = response['user']['code'];

          }
          return response;
        })
      );
  }

  register(
    name: string,
    surname:string,
    email: string,
    password: string,
    password_confirmation: string,
    pais_id: string
    ): Observable < any > {
    return this.http.post(this.registerEndPoint, {
        name: name,
        surname: surname,
        email: email,
        password: password,
        password_confirmation: password_confirmation,
        pais_id: pais_id
      }, {
        headers: this.headers
      })
      .pipe(
        map((response: Response) => {
          this.token = response['access_token'];
          let email = response['email'];
          if (this.token) {
            localStorage.setItem('user',
              JSON.stringify({
                email: email,
                token: this.token
              }));
          }
          return response;
        })
      );
  }

  logout() {
    this.u.removeTokenUser();
  }

  sendPasswordResetEmail(email: string): Observable < any > {
    return this.http.post(this.sendPasswordEndPonit, {
        email: email,
        url: this.resetPasswordEndPoint
      }, {
        headers: this.headers
      })
      .pipe(
        map((response: Response) => {
          return response;
        })
      );
  }

  resetPassword(newPassword: string, confirmedPassword: string, token: string): Observable < any > {
    return this.http.post(this.resetPasswordEndPoint, {
        password: newPassword,
        confirm_password: confirmedPassword,
        token: token
      }, {
        headers: this.headers
      })
      .pipe(
        map((response: Response) => {
          return response;
        })
      );
  }
}
