import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { GuardServiceProvider } from "../guard-service/guard-service";
import { AuthServiceProvider } from "../auth-service/auth-service";
import { UsuarioProvider } from "../usuario/usuario";

@Injectable()
export class ApiProvider {

  public headers;
  url_base_api: string = "";

  constructor(public http: HttpClient, 
              public guard: GuardServiceProvider,
              public auth: AuthServiceProvider,
              public us: UsuarioProvider) {

    this.url_base_api = this.auth.baseURL;
  }

  post(ruta: string, parametros: any): Observable<any> {
    this.headers = {
      'Authorization': 'Bearer ' + this.us.getTokenUser()
    };
    
    return this.http.post(this.url_base_api + ruta, parametros, {
      headers: this.headers
    });
  }
  
}
