import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { LISTA_STATUS } from "../../data/status.data";
import { LISTA_VEHICULOS } from "../../data/vehiculos.data";
import { TIPOS_PAGOS } from "../../data/tipos_pagos.data";

@Injectable()
export class ConfiguracionesProvider {
  
  listaStatus = LISTA_STATUS;
  listaVehiculos = LISTA_VEHICULOS;
  listaTiposPagos = TIPOS_PAGOS;

  constructor(public http: HttpClient) {}

  getListaStatus(){
    return this.listaStatus;
  }

  getListaVehiculos() {
    return this.listaVehiculos;
  }
  
  getListaTiposPagos() {
    return this.listaTiposPagos;
  }

  getInfoVehiculo($id: number){
    let $retornar;

    this.listaVehiculos.map(function(vehiculo){
      if(vehiculo.id_tipo == $id)
        $retornar = vehiculo;
    });

    return $retornar;
  }

  getInfoStatus($key: number){
    return this.listaStatus[$key];
  }

  _getImgVehiculo($tipo_vehiculo_id:number){
    let $vehiculo = this.getInfoVehiculo($tipo_vehiculo_id);
    return '../../assets/imgs/tipos_vehiculos/' + $vehiculo.logo;
  }
}
