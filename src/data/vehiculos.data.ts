export const LISTA_VEHICULOS = [
    {
        id_tipo: 1,
        nombre: 'Economy',
        tiempo: '10 Mins',
        logo: 'economy.png',
        factor_precio: 1.2,
        seleccionado: false,
    },
    {
        id_tipo: 2,
        nombre: 'Lite',
        tiempo: '10 Mins',
        logo: 'lite.png',
        factor_precio: 1.1,
        seleccionado: false,
    }
]