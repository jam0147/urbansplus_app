export const LISTA_STATUS = [
    {
        "nombre": "Solicitud Enviada",
        "clase" : "solicitud_enviada"
    },
    {
        "nombre": "Solicitud Cancelada",
        "clase": "solicitud_cancelada"
    },
    {
        "nombre": "Solicitud Aceptada por Conductor",
        "clase": "solicitud_aceptada"
    },
    {
        "nombre": "Viaje Finalizado",
        "clase": "solicitud_finalizada"
    }
]