export const TIPOS_PAGOS = [
    {
        id: 1,
        nombre: 'Efectivo',
        logo: 'cash',
        activo: true
    },
    {
        id: 2,
        nombre: 'Paypal',
        logo: 'ruta',
        activo: true
    },
    {
        id: 3,
        nombre: 'Tarjeta de Crédito',
        logo: 'card',
        activo: false
    }
]